#Alejandro E. Rendon <alejorendon@utp.edu.co>
#Inteligencia Artificial
#Ingenieria de Sistemas y Computacion
#Universidad Tecnologica de Pereira

#Algoritmo DFS

grafo = [[2], [0], [], [1, 2, 4], [7, 10], [2, 6], [], [9], [9], [], [5], [3, 4, 8]]
visitados = [11]
ruta = [11]

def dfs(grafo, x, objetivo):
	bandera = False

	for y in grafo[x]:
		if y not in visitados and not bandera:
			visitados.append(y)
			ruta.append(y)
			if y == objetivo:
				return True
			else:
				bandera = dfs(grafo, y, objetivo)
				if not bandera:
					ruta.pop()
	return bandera

dfs(grafo, 11, 6)
print ruta