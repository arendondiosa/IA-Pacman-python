# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        return successorGameState.getScore()

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    #Esta funcion nos permite maximizar los niveles del arbol

    def maximize(self, gameState, depth, ghosts):

      #En la funcion maximize, somos el PACMAN y queremos maximizar nuestras posibilidades de ganar

      #Iniciamos el indicador para maximos en el MINIMO valor posible
      maxvalue= float("-inf")
      #Si el estado actual es una hoja... Retornamos el valor de dicho estado
      if gameState.isWin() or gameState.isLose():
        return self.evaluationFunction(gameState)

      #Si aun no hemos llegado al final, exploramos nuestras posibilidades
      #Y se proceden a minimizar
      for posibilities in gameState.getLegalActions(0):
        #Generamos el estado sucesor por cada accion valida posible
        #Estando desde nuestro estado actual
        successor = gameState.generateSuccessor(0, posibilities)
        
        #Corremos el proceso minimizado para compararlo con nuestros otros estados
        #Usamos el sucesor actual, indicamos la profundidad y el indice para el agente determinado
        temp = self.minimize(successor, depth, 1)
        if temp > maxvalue:
          maxvalue = temp
          bestAction = posibilities

      #Esta funcion es llamada desde getAction(), por lo tanto, si es parte del proceso recursivo, retornara el valor
      #Correspondiente al maximo en el analisis del nivel actual
      #Si estamos en la ultima etapa del proceso, debemos retornar la accion que maximice mis posibilidades de ganar!
      #La cual se asocia en el momento en que el temp mejora las posibilidades de ganar que el maxvalue actual

      if depth == 1:
        return bestAction
      else:
        return maxvalue

    def minimize(self, gameState, depth, ghosts):

      #En la funcion minimize, somos los fantasman y vamos a minimizar las posibilidades de ganar del PACMAN
      #Aqui si importan la cantridad de agentes en un momento determinado

      #Iniciamos el indicador para minimos en el MAXIMO valor posible
      minvalue= float("inf")

      #Debemos conocer la cantidad de agentes que tenemos en un momento determinado para hacer el analisis
      totalAgents = gameState.getNumAgents()

      #Si aun no hemos llegado al final, exploramos nuestras posibilidades
      #Y se proceden a minimizar
      if gameState.isWin() or gameState.isLose():
        return self.evaluationFunction(gameState)

      for posibilities in gameState.getLegalActions(ghosts):
        successor = gameState.generateSuccessor(ghosts, posibilities)

        # Si el agente que estamos revisando es el ultimo en la lista para revision
        if ghosts == totalAgents - 1:
          #El arbol de expansion lo ejecutamos hasta cierta profundidad (de lo contrario seria muy lento), Retornamos en este caso
          #Esta definicion se realiza cuando se invoca al pacman desde linea de comandos

          if depth == self.depth:
            temp = self.evaluationFunction(successor)
          else:
            #Procedemos a cambiar de contexto, maximizando las posibilidades que nos dejan los fantasmas aumentando la profundidad
            #Aqui no importan la cantidad de fantasmas
            temp = self.maximize(successor, depth+1, 0)
        #Sin embargo, si aun no hemos terminado de minimizar con los fantasmas, seguimos minimizando con los siguientes agentes
        else:
          temp = self.minimize(successor, depth, ghosts+1)

        #Nuevamente conservamos el minimo y por consiguiente la accion correspondiente a ese estado
        if temp < minvalue:
          minvalue = temp
          minAction = posibilities

      #Retornamos el minimo
      return minvalue

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"

        # Iniciamos el arbol maximizando
        best = self.maximize(gameState, 1, 0)
        return best
        util.raiseNotDefined()

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

