# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from util import nearestPoint
from game import Directions
import game


#---------------------------------------------------------------------------------------------------------
#Vamos a definir en primera instancia una clase que nos permita hacer una busqueda A*
#Eventualmente seria mas sencillo implementar un Dijsktra normal sin embargo, usar una heuristica
#Nos permite reducir el tiempo (cuidando tambien la efectividad) que tarda en computarse las operaciones

class Node:
  def __init__(self, state, action, cost, parent):
    self.state = state
    self.action = action
    self.cost = cost
    self.parent = parent
    
  def getPath(self):
    if self.parent == None: return []
    path = self.parent.getPath()
    return path + [self.action] 

#---------------------------------------------------------------------------------------------------------

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'TopAgent', second = 'BottomAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class MainAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on). 
    
    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    ''' 
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py. 
    '''
    CaptureAgent.registerInitialState(self, gameState)

    ''' 
    Your initialization code goes here, if you need any.
    '''

    #Para independizar los agentes y poder jugar como Azul o Rojo realizamos el ajuste 
    #Capturando los indices seguirlos usando de forma generica mas adelante
    if self.red:
      CaptureAgent.registerTeam(self, gameState.getRedTeamIndices())
    else:
      CaptureAgent.registerTeam(self, gameState.getBlueTeamIndices())

    #Pensamos controlar el mediocampo representa una ventaja fundamental por lo cual como primera medida
    #Los agentes capturaran las pocisiones centrales al comenzar la partida!
    self.goToCenter(gameState)

  #-------------------------------------------------------------------------------------------------------
  #A partir de aqui se definen algunos metodos utiles que se usaran a lo largo de la implementacion

  #Retorna una lista con los enemigos visibles (Puede llegar a retornar vacio!!)
  def getEnemyPos(self, gameState):
    enemyPos = []
    for enemyI in self.getOpponents(gameState):
      pos = gameState.getAgentPosition(enemyI)
      #Si no se reporta la posicion hay que inferirla (mas adelante)
      if pos != None:
        enemyPos.append((enemyI, pos))
    return enemyPos

  #Retorna la distancia que hay al enemigo mas cercano (Puede llegar a retornar None!!)
  def enemyDist(self, gameState):
    #Averiguamos los enemigos visibles
    pos = self.getEnemyPos(gameState)
    minDist = None
    if len(pos) > 0:
      #Inicializamos en un valor grande para calcular los minimos
      minDist = float('inf')
      #Registramos nuestra posicion actual (recordar que cada instancia de MainAgent es un agente)
      myPos = gameState.getAgentPosition(self.index)
      #Si esto es correcto, como maximo deberia recorrerse 2 veces (dos enemigos)
      for i, p in pos:
        dist = self.getMazeDistance(p, myPos)
        if dist < minDist:
          minDist = dist
    return minDist

  #Este metodo nos permite determinar si somos un pacman o un fantasma
  #Analogamente si estamos en territorio enemigo o en territorio nuestro
  def inEnemyTerritory(self, gameState):
    return gameState.getAgentState(self.index).isPacman

  #Esta es la definicion para la A* basada en la A* de la primera implementacion
  def aStarSearch(self, problem):
    startNode = Node(problem.getStartState(), None, 0, None)
    if problem.isGoalState(startNode.state): return []
    frontier = util.PriorityQueue()
    frontier.push(startNode, self.heuristic(startNode.state, problem))
    explored = set()
    while True:
      if frontier.isEmpty(): return None
      node = frontier.pop()
      if node.state in explored: continue
      if problem.isGoalState(node.state): return node.getPath()
      explored.add(node.state)
      children = problem.getSuccessors(node.state)
      for (st, act, cst) in children:
        child = Node(st, act, cst+node.cost, node)
        if child.state not in explored:
          frontier.push(child, self.heuristic(child.state, problem))

  #Aqui definimos la busqueda A* (para la cual creamos la clase node!!)
  def heuristic(self, state, problem):
    #Registramos nuestra posicion
    position = state.getAgentPosition(self.index)  
    #Registramos la posicion de las galletas                
    food = self.getFood(state).asList()
    #Registramos la posicion de los muros
    walls = problem.walls

    #Inicializamos la cuenta en 0
    heur = 0
    c = None
    #Vamos a conservar la comida mas cercana!
    for fd in food:
      x = self.getMazeDistance(fd, position)
      if c == None or x < self.getMazeDistance(c, position): c = fd
    #Si no logramos registrar ninguna retornamos heur (El cual deberia valer 0)
    #En teoria este estado no deberia ser alcanzable pero es mejor prevenir
    if c == None: return heur
    heur = self.getMazeDistance(c, position)
    #Vamos a matchear las distancias para asignar un valor tentativo a una ruta y comer las galletas
    if len(food) > 1:
      for fd in food:
        if fd != c:
          #Por ser un valor entero no usamos float('inf')!!
          closest = 99999
          for nxt in food:
            dist = self.getMazeDistance(fd, nxt)
            if nxt != fd and dist < closest:
              closest = dist
          heur+=closest
    return heur

  #--------------------------------------------Fin de auxiliares--------------------------------------------
  

  def chooseAction(self, gameState):
    # You can profile your evaluation time by uncommenting these lines
    # start = time.time()

    #Obetenos la pocision actual
    agentPos = gameState.getAgentPosition(self.index)

    #Inicializamos la estrategia en ataque
    evaluateType = 'attack'

    #Siempre nos mantendremos en start hasta no alcanzar el centro por primera vez.
    if self.atCenter == False:
      evaluateType = 'start'

    #En el momento de lograrlo proseguimos a la ofensiva
    if agentPos == self.center and self.atCenter == False:
      self.atCenter = True
      evaluateType = 'attack'

    #Consultamos la cantidad de enemigos avistados
    enemyPos = self.getEnemyPos(gameState)

    #Iteraremos a traves de los detectados y los perseguiremos
    if len(enemyPos) > 0:
      for enemyI, pos in enemyPos:
        #Si se detecta un enemigo invadiendo procedemos a defender
        if self.getMazeDistance(agentPos, pos) < 6 and not self.inEnemyTerritory(gameState):
          evaluateType = 'defend'
          break

    #Computamos las posibilidades de movimiento
    actions = gameState.getLegalActions(self.index)

    #Evaluamos las posibilidades teniendo en cuenta nuestra situacion actual
    values = [self.evaluate(gameState, a, evaluateType) for a in actions]

    #Seleccionamos el valor de la mejor opcion
    maxValue = max(values)

    #Podria elegirse siempre la primera mejor opcion pero...
    #Para ser mas impredecibles tomaremos todas las mejores y probaremos al azar
    bestActions = [a for a, v in zip(actions, values) if v == maxValue]

    return random.choice(bestActions)

  def getSuccessor(self, gameState, action):
    
    #Con este metodo podemos generar las tuplas sucesoras de un estado
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Significa que no es una pocision del tablero valida!! Buscar otra!!
      return successor.generateSuccessor(self.index, action)
    else:
      return successor


  def evaluate(self, gameState, action, evaluateType):
    
    #Realizamos una combinacion lineal entre las caracteristicas y los pesos de cada una
    if evaluateType == 'attack':
      features = self.getFeaturesAttack(gameState, action)
      weights = self.getWeightsAttack(gameState, action)
    elif evaluateType == 'defend':
      features = self.getFeaturesDefend(gameState, action)
      weights = self.getWeightsDefend(gameState, action)
    elif evaluateType == 'start':
      features = self.getFeaturesStart(gameState, action)
      weights = self.getWeightsStart(gameState, action)

    return features * weights

  
  def getFeaturesAttack(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)

    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    #Calculamos la distancia a la comida mas cercana
    foodList = self.getFood(successor).asList()

    if len(foodList) > 0: # Esta condicion siempre deberia ser verdadera... Pero por si las moscas
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
      features['distanceToFood'] = minDistance

    #Compute distance to ally agent (maximize distance between if in enemyTerritory)
 
    #Revisamos si nos escontramos cerca a un enemigo... En dicho caso entramos en estado de peligro
    distEnemy = self.enemyDist(successor)
    if(distEnemy <= 4):
      features['danger'] = 1
    else:
      features['danger'] = 0  

    #Hallamos la distancia a la capsula mas cercana
    capsules = self.getCapsules(successor)
    if(len(capsules) > 0):
      minCapsuleDist = min([self.getMazeDistance(myPos, capsule) for capsule in capsules])
    else:
      minCapsuleDist = .1
      
    features['capsuleDist'] =  1.0 / minCapsuleDist

    #Solo salvo casos excepcionales retroceder es buena opcion (a menos que este en peligro)
    #Por esta razon en los pesos se castiga dicha decision
    #Lo mismo ocurre con detenerse.
    if action == Directions.STOP: features['stop'] = 1
    rev = Directions.REVERSE[gameState.getAgentState(self.index).configuration.direction]
    if action == rev: features['reverse'] = 1

    return features

  def getFeaturesDefend(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)

    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    #Vamos a revisar la distancia que hay con los invasores visibles
    enemies = [successor.getAgentState(i) for i in self.getOpponents(successor)]
    invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
    
    #En len nos otorga la cantidad de invasores
    features['numInvaders'] = len(invaders)
    if len(invaders) > 0:
      dists = [self.getMazeDistance(myPos, a.getPosition()) for a in invaders]
      #Si ajustamos un peso negativo, una mayor distancia castigara alejarnos del invasor
      features['invaderDistance'] = min(dists)

    #La misma apreciacion con la funcion de ataque
    if action == Directions.STOP: features['stop'] = 1
    rev = Directions.REVERSE[gameState.getAgentState(self.index).configuration.direction]
    if action == rev: features['reverse'] = 1

    return features

  def getFeaturesStart(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)

    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    dist = self.getMazeDistance(myPos, self.center)

    #En este caso, solo nos concentramos en acercarnos al centro y nada mas.
    features['distToCenter'] = dist
    if myPos == self.center:
      features['atCenter'] = 1
    return features


  def getWeightsAttack(self, gameState, action):
    return {'successorScore': 100, 'danger': -400, 'distanceToFood': -1, 'stop': -2000, 'reverse': -20, 'capsuleDist': 3}


  def getWeightsDefend(self, gameState, action):
    return {'numInvaders': -1000, 'invaderDistance': -50, 'stop': -2000, 'reverse': -20}


  def getWeightsStart(self, gameState, action):
    return {'distToCenter': -1, 'atCenter': 1000}


class TopAgent(MainAgent):

  def goToCenter(self, gameState):
    locations = []
    self.atCenter = False
    x = gameState.getWalls().width / 2
    y = gameState.getWalls().height / 2
    #0 to x-1 and x to width
    if self.red:
      x = x - 1
    self.center = (x, y)
    maxHeight = gameState.getWalls().height

    #look for locations to move to that are not walls (favor top positions)
    for i in xrange(maxHeight - y):
      if not gameState.hasWall(x, y):
        locations.append((x, y))
      y = y + 1

    myPos = gameState.getAgentState(self.index).getPosition()
    minDist = float('inf')
    minPos = None

    for location in locations:
      dist = self.getMazeDistance(myPos, location)
      if dist <= minDist:
        minDist = dist
        minPos = location
    
    self.center = minPos


class BottomAgent(MainAgent):

  def goToCenter(self, gameState):
    locations = []
    self.atCenter = False
    x = gameState.getWalls().width / 2
    y = gameState.getWalls().height / 2
    #0 to x-1 and x to width
    if self.red:
      x = x - 1
    self.center = (x, y)
    
    #look for locations to move to that are not walls (favor bot positions)
    for i in xrange(y):
      if not gameState.hasWall(x, y):
        locations.append((x, y))
      y = y - 1

    myPos = gameState.getAgentState(self.index).getPosition()
    minDist = float('inf')
    minPos = None

    for location in locations:
      dist = self.getMazeDistance(myPos, location)
      if dist <= minDist:
        minDist = dist
        minPos = location
    
    self.center = minPos
