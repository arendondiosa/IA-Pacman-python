graph = {'A': ['B', 'C','D'],
         'B': ['A', 'D', 'E'],
         'C': ['A', 'F'],
         'D': ['B'],
         'E': ['B', 'F'],
         'F': ['C', 'E']}

graph2= {'A': ['B', 'F',],
         'B': ['A', 'C', 'D', 'E'],
         'C': ['B', 'E', 'H'],
         'D': ['B', 'E', 'F'],
         'E': ['B', 'C', 'D', 'F', 'G', 'H'],
         'F': ['A', 'D', 'E', 'G'],
         'G': ['E', 'F'],
         'H': ['C', 'E']}
    


def bfs(G, i, t):
    q = []
    q.append(i)
    #We use a set because it has fast method to search an element in itself
    visit = set()
    while q:
    	path = q.pop(0)
        #print path
        #Using -1 because is a reverse method to explore the list being -1 the last element added
        current = path[-1]
        visit.add(current)

        #Returns the path if the goal t is reached
        if current == t:
        	return path

        #Explore de adjacency list. We use "get" that returns [] if the key isn't founded
        for adj in G.get(current, []):
            #Queue the elements not visited
	        if adj not in visit:
                #Create a new temporally list to expand the current path
	        	temp = list(path)
	        	temp.append(adj)
	        	q.append(temp)


ans = bfs(graph2, 'A', 'H')
#ans = bfs(graph2, 'A', 'G')
print "Ruta: ", ans, "Distancia: ", len(ans)