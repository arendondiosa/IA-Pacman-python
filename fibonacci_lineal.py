#Alejandro E. Rendon <alejorendon@utp.edu.co>
#Inteligencia Artificial
#Fibonacci Lineal
#UNIVERSIDAD TECNOLOGICA DE PEREIRA
#2.015

vec = []
vec.append(0)
vec.append(1)

for i in range(2, 301):
	vec.append(vec[i-2]+vec[i-1])

#Para imprimir los enteros largos se convierten a cadenas
for i in range(len(vec)):
	vec[i] = str(vec[i])
	print "Fibonacci de ", i," es: ", vec[i] 