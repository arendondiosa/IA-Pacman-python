%Alejandro Esteban Rend�n Diosa
%Jonatan Gutierrez Obando


label = loadMNISTLabels('t10k-labels.idx1-ubyte');  
images = loadMNISTImages('t10k-images.idx3-ubyte');
%labelTrain = loadMNISTLabels('train-labels.idx1-ubyte');  
%imagesTrain = loadMNISTImages('train-images.idx3-ubyte');
label_training = label(2001:10000, :);
label_test = label(1:2000, :);

%imshow(reshape(imagesTrain(:,#imagen), [28, 28]));  -----Para mostrar un
%dato en el dataset

x = [ones(1, size(images, 2)); images];
x_training = x(:, 2001:10000);
x_test = x(:, 1:2000);



%Fordward
epsilon = 5;
NN = 300;
ND = 10;
W1 = rand(size(x_training, 1), NN)*(2*epsilon)-epsilon;
W2 = rand(301, ND)*(2*epsilon)-epsilon;
epsilon = 5;

f = inline('1 ./ (1+exp(-x))');  %funcion Sigmoidal
%plot(-2:0.1:2, f(-2:0.1:2));

fp = inline('(1./(1+exp(-x)) ).* (1 -(1./(1+exp(-x))) ) '); %Derivada de la Funci�n Sigmoidal
alpha = 0.1;
m = 8000;  %Muestras de prueba
j=[];

y = zeros(10, 8000);

for i = 1:8000
    y(label_training(i)+1, i) = 1;
end

for j = 1:500
    Z1 = W1'*x_training;
    
    a1 = f(Z1);
    a1 = [ones(1, size(a1, 2)); a1];
    
    Z2 = W2'*a1;
    a2 = f(Z2);

    %Back

    d3 = (y - a2).* fp(a2);
    d2 = (W2*d3).* fp(a1);
    dW2 = a1 * d3';
    dW1 = x_training * d2(2:end, :)';

    %Actualizaci�n de pesos

    W1 = W1 - (alpha / m)* dW1;
    W2 = W2 - (alpha / m)* dW2;
    m_aux = (a2 - y).^2;
    j_aux = (sum(m_aux,2)) / (2*m);
    %j = [j; i,j_aux] ; 
end
    
figure
hold on
plot(j_aux,'r')